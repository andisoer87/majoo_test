import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/register/register_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit()..fetchMovieData(),
                    child: HomeBlocScreen(),
                  ),
                ),
                (route) => false);
          } else if (state is AuthBlocErrorState) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.red,
                content: Text('Login gagal , periksa kembali inputan anda'),
              ),
            );
          }
        },
        child: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 32,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                _buildForm(),
                SizedBox(
                  height: 35,
                ),
                CustomPrimaryButton(
                  label: 'Login',
                  onPressed: () {
                    _handleLogin();
                  },
                ),
                SizedBox(height: 25),
                Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width * 1 / 2,
                    child: Divider(
                      thickness: 1,
                      color: Colors.grey.withOpacity(0.5),
                    ),
                  ),
                ),
                _buildRegisterButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          CustomTextFormField(
            controller: _emailController,
            textInputType: TextInputType.emailAddress,
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid';
            },
            hintText: 'your@email.com',
          ),
          SizedBox(
            height: 12,
          ),
          CustomTextFormField(
            controller: _passwordController,
            isObscurePassword: _isObscurePassword,
            validator: (val) {
              if (val == null || val.isEmpty) return 'Form tidak boleh kosong';
            },
            hintText: 'password',
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRegisterButton() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => BlocProvider(
                create: (context) => AuthBlocCubit(),
                child: RegisterPage(),
              ),
            ),
          );
        },
        child: RichText(
          text: TextSpan(
            text: 'Belum punya akun? ',
            style: TextStyle(color: Colors.black45),
            children: [
              TextSpan(text: 'Daftar', style: TextStyle(color: Colors.black)),
            ],
          ),
        ),
      ),
    );
  }

  void _handleLogin() async {
    String _email = _emailController.text;
    String _password = _passwordController.text;
    if (_formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      _email = _email.trim();
      _password = _password.trim();

      var authBloc = BlocProvider.of<AuthBlocCubit>(context);
      authBloc.loginUser(_email, _password);
    }
  }
}
