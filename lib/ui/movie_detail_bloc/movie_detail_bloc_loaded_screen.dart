import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_detail_response.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/date_helper.dart';

class MovieDetailBlocLoadedScreen extends StatelessWidget {
  const MovieDetailBlocLoadedScreen({
    Key key,
    @required this.movie,
  }) : super(key: key);

  final MovieDetailResponse movie;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: _buildMovieInformation(context),
    );
  }

  Stack _buildMovieInformation(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Image.network(Api.URL_BACKDROP_W780 + movie.backdropPath),
        _buildBackButton(context),
        Container(
          margin: EdgeInsets.only(top: 200),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(24),
                topRight: Radius.circular(24),
              ),
              boxShadow: [
                BoxShadow(
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(0, -5),
                  color: Colors.grey.withAlpha(70),
                )
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(24),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(
                            Radius.circular(24),
                          ),
                          child: Image.network(
                            Api.URL_POSTER_W154 + movie.posterPath,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(top: 16, left: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                movie.title,
                                style: TextStyle(
                                  fontSize: 32,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8),
                                child: Text(
                                  formatDate("dd MMMM yyyy", movie.releaseDate),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.star,
                                      color: Colors.orange,
                                    ),
                                    Text(
                                      movie.voteAverage.toString(),
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  _buildOverview(),
                  _buildGenreList(),
                  _buildProductionCompaniesList(),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  SafeArea _buildBackButton(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              blurRadius: 2,
              spreadRadius: 2,
              color: Colors.grey.withAlpha(70),
            )
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(36),
          ),
        ),
        child: IconButton(
          padding: EdgeInsets.all(0),
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            size: 18,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  Column _buildOverview() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 10, top: 24),
          child: Text(
            'Overview',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),
          ),
        ),
        Text(movie.overview),
      ],
    );
  }

  Column _buildProductionCompaniesList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 10, top: 24),
          child: Text(
            'Production Companies',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),
          ),
        ),
        Container(
          height: 30,
          child: ListView.builder(
            clipBehavior: Clip.none,
            itemCount: movie.productionCompanies.length,
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                  color: Colors.grey.withAlpha(80),
                ),
                margin: EdgeInsets.only(right: 8),
                padding: EdgeInsets.all(8),
                child: Text(
                  movie.productionCompanies[index].name,
                  style: TextStyle(color: Colors.black),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Column _buildGenreList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 10, top: 24),
          child: Text(
            'Genres',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),
          ),
        ),
        Container(
          height: 30,
          child: ListView.builder(
            clipBehavior: Clip.none,
            itemCount: movie.genres.length,
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                  color: Colors.grey.withAlpha(80),
                ),
                margin: EdgeInsets.only(right: 8),
                padding: EdgeInsets.all(8),
                child: Text(
                  movie.genres[index].name,
                  style: TextStyle(color: Colors.black),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
