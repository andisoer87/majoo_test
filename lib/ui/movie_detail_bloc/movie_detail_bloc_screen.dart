import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/movie_detail_bloc/movie_detail_bloc_cubit.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/movie_detail_bloc/movie_detail_bloc_loaded_screen.dart';

class MovieDetailBlocScreen extends StatefulWidget {
  final int movieId;
  MovieDetailBlocScreen({this.movieId, key}) : super(key: key);

  @override
  State<MovieDetailBlocScreen> createState() =>
      _MovieDetailBlocScreenState(movieId: movieId);
}

class _MovieDetailBlocScreenState extends State<MovieDetailBlocScreen> {
  int movieId;
  _MovieDetailBlocScreenState({this.movieId});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocBuilder<MovieDetailBlocCubit, MovieDetailBlocState>(
        builder: (context, state) {
          if (state is MovieDetailBlocLoadedState) {
            var movie = state.movieDetail;
            return MovieDetailBlocLoadedScreen(movie: movie);
          } else if (state is MovieDetailBlocLoadingState) {
            return LoadingIndicator();
          } else if (state is MovieDetailBlocErrorState) {
            return ErrorScreen(
              message: state.error,
              fontSize: 24,
              retry: () {
                _handleRefresh(context);
              },
            );
          }

          return Center(
              child: Text(kDebugMode ? "state not implemented $state" : ""));
        },
      ),
    );
  }

  _handleRefresh(BuildContext context) {
    BlocProvider.of<MovieDetailBlocCubit>(context).fetchMovieDetail(movieId);
  }
}
