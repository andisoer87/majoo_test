import 'package:flutter/material.dart';

class EmptyDataScreen extends StatelessWidget {
  const EmptyDataScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text("No data found"),
    );
  }
}
