import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.green,
                content: Text('Register Berhasil'),
              ),
            );
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit()..fetchMovieData(),
                    child: HomeBlocScreen(),
                  ),
                ),
                (route) => false);
          } else if (state is AuthBlocErrorState) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.red,
                content: Text('Login gagal , periksa kembali inputan anda'),
              ),
            );
          }
        },
        child: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 32,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan registrasi terlebih dahulu',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                _buildForm(),
                SizedBox(
                  height: 50,
                ),
                CustomPrimaryButton(
                  label: 'Registrasi',
                  onPressed: () {
                    _handleRegister();
                  },
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            controller: _usernameController,
            validator: (val) {
              if (val == null || val.isEmpty) return 'Form tidak boleh kosong';
            },
            hintText: 'username',
          ),
          SizedBox(height: 12),
          CustomTextFormField(
            controller: _emailController,
            textInputType: TextInputType.emailAddress,
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid';
            },
            hintText: 'your@email.com',
          ),
          SizedBox(height: 12),
          CustomTextFormField(
            controller: _passwordController,
            isObscurePassword: _isObscurePassword,
            validator: (val) {
              if (val == null || val.isEmpty) return 'Form tidak boleh kosong';
            },
            hintText: 'password',
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void _handleRegister() async {
    String _username = _usernameController.text;
    String _email = _emailController.text;
    String _password = _passwordController.text;
    if (formKey.currentState?.validate() == true &&
        _username != null &&
        _email != null &&
        _password != null) {
      _username = _username.trim();
      _email = _email.trim();
      _password = _password.trim();

      var authBloc = BlocProvider.of<AuthBlocCubit>(context);
      authBloc.registerUser(_username, _email, _password);
    }
  }
}
