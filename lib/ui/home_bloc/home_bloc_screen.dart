import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/ui/extra/empty_screen.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatefulWidget {
  const HomeBlocScreen({Key key}) : super(key: key);

  @override
  _HomeBlocScreenState createState() => _HomeBlocScreenState();
}

class _HomeBlocScreenState extends State<HomeBlocScreen> {
  var order = 'asc';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: 32,
                  bottom: 16,
                  left: 16,
                  right: 16,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Movie List',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 36,
                          ),
                        ),
                        Text(
                          'Daftar film pilihan',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.logout,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        _handleLogout(context);
                      },
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
                child: Row(
                  children: [
                    Flexible(
                      child: CustomTextFormField(
                        hintText: 'Cari film..',
                        suffixIcon: Icon(
                          Icons.search,
                          color: Colors.grey,
                        ),
                        fillColor: Colors.grey.withOpacity(0.2),
                        onFieldSubmitted: (value) {
                          _submitSearchQuery(context, value);
                        },
                        textInputAction: TextInputAction.search,
                      ),
                    ),
                    IconButton(
                      color: Colors.grey,
                      icon: Icon(
                        Icons.sort_by_alpha,
                      ),
                      onPressed: () {
                        _handleSorting(context);
                      },
                    )
                  ],
                ),
              ),
              BlocBuilder<HomeBlocCubit, HomeBlocState>(
                builder: (context, state) {
                  log(state.toString());
                  if (state is HomeBlocLoadedState) {
                    return HomeBlocLoadedScreen(data: state.data);
                  } else if (state is HomeBlocLoadingState) {
                    return LoadingIndicator();
                  } else if (state is HomeBlocInitialState) {
                    return EmptyDataScreen();
                  } else if (state is HomeBlocErrorState) {
                    return ErrorScreen(
                      message: state.error,
                      fontSize: 24,
                      retry: () {
                        _handleRefresh(context);
                      },
                    );
                  } else if (state is HomeBlocEmptyState) {
                    return ErrorScreen(
                      message: state.message,
                      fontSize: 24,
                    );
                  }

                  return Center(
                      child: Text(
                          kDebugMode ? "state not implemented $state" : ""));
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void _handleLogout(BuildContext context) {
    AuthBlocCubit authBlocCubit = AuthBlocCubit();
    authBlocCubit.clearUserSession();

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (context) => AuthBlocCubit()..fetchLoginSession(),
          child: MyHomePageScreen(),
        ),
      ),
      (route) => false,
    );
  }

  _handleRefresh(BuildContext context) {
    BlocProvider.of<HomeBlocCubit>(context).fetchMovieData();
  }

  void _submitSearchQuery(BuildContext context, String query) async {
    var movieBloc = BlocProvider.of<HomeBlocCubit>(context);
    movieBloc.searchMovie(query);
  }

  void _handleSorting(BuildContext context) {
    setState(() {
      if (order == 'asc') {
        order = 'desc';
      } else {
        order = 'asc';
      }
    });
    var movieBloc = BlocProvider.of<HomeBlocCubit>(context);
    movieBloc.sortMovieList(order);
  }
}
