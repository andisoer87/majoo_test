class User {
  int id;
  String email;
  String username;
  String password;

  User({this.id, this.email, this.username, this.password});

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        password = json['password'],
        username = json['username'];

  Map<String, dynamic> toJson() =>
      {'id': id, 'email': email, 'password': password, 'username': username};

  Map<String, Object> toMap() {
    var map = <String, Object>{
      'email': email,
      'username': username,
      'password': password,
    };
    if (id != null) {
      map['id'] = id;
    }
    return map;
  }

  User.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        email = map['email'],
        password = map['password'],
        username = map['username'];
}
