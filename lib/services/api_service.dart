import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_detail_response.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

/// Class berfungsi untuk ambil operasi dari api
/// Author @ Andi Surya
/// Hasil berupa response dari api
class ApiServices {

  //ambil api now_playing
  Future<MovieResponse> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response = await dio.get("now_playing");
      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data));
      return movieResponse;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //ambil api detail movie
  Future<MovieDetailResponse> getMovieDetail(int movieId) async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response = await dio.get(movieId.toString());
      MovieDetailResponse movieResponse =
          MovieDetailResponse.fromJson(jsonDecode(response.data));
      return movieResponse;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
