import 'dart:async';
import 'package:dio/dio.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio dioInstance;
createInstance() async {
  var options = BaseOptions(
    baseUrl: Api.BASE_URL,
    connectTimeout: 12000,
    receiveTimeout: 12000,
    headers: {
      "x-rapidapi-key": "6af4f77398mshb3f6cd027729468p1d6d31jsn038947888a17",
      "x-rapidapi-host": "imdb8.p.rapidapi.com"
    },
    queryParameters: {"api_key": Api.API_KEY}
  );
  dioInstance = new Dio(options);
  dioInstance.interceptors.add(PrettyDioLogger(
    requestHeader: true,
    requestBody: true,
    responseBody: true,
    responseHeader: false,
    error: true,
    compact: true,
    maxWidth: 90,
  ));
}

Future<Dio> dio() async {
  await createInstance();

  return dioInstance;
}
