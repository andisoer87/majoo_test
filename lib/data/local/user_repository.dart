import 'package:majootestcase/data/local/user_dao.dart';
import 'package:majootestcase/data/local/user_data_source.dart';
import 'package:majootestcase/models/user.dart';

class UserRepository extends UserDataSource {
  var userDao = UserDao();

  @override
  Future<User> login(String email, String password) {
    return userDao.login(email, password);
  }

  @override
  Future<int> register(String username, String email, String password) {
    return userDao.register(username, email, password);
  }
}
