import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/data/local/db_provider.dart';

/// Class berfungsi untuk ambil operasi dari database
/// Author @ Andi Surya
/// Hasil berupa manipulasi data dari table users
class UserDao {
  static final _tableUser = 'users';

  final dbProvider = DatabaseProvider.databaseProvider;

  //register user ke database
  Future<int> register(
    String username,
    String email,
    String password,
  ) async {
    var user = User(username: username, email: email, password: password);

    final db = await dbProvider.database;
    var result = db.insert(_tableUser, user.toMap());

    return result;
  }

  //login user dari database
  Future<User> login(
    String email,
    String password,
  ) async {
    final db = await dbProvider.database;
    List<Map> result = await db.query(_tableUser,
        columns: ['id', 'username', 'email', 'password'],
        where: 'email = ? and password = ?',
        whereArgs: [email, password]);

    if (result.isNotEmpty) {
      var user = User.fromMap(result.first);
      return user;
    } else {
      return null;
    }
  }
}
