import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

/// Class berfungsi untuk inisialisasi database
/// Author @ Andi Surya
/// Hasil berupa manupulasi data user
class DatabaseProvider {
  static final DatabaseProvider databaseProvider = DatabaseProvider();

  Database _database;
  
  //Ambil instance _database
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _createDatabase();
    return _database;
  }

  //load dan open koneksi local database
  _createDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "db_movies.db");

    var database = await openDatabase(path,
        version: 1, onCreate: _initDB, onUpgrade: _onUpgrade);
    return database;
  }

  //inisialisasi database dengan membuat table users pertama kali 
  void _initDB(Database database, int version) async {
    await database.execute("CREATE TABLE 'users' ("
        "id INTEGER PRIMARY KEY,"
        "email VARCHAR,"
        "username VARCHAR,"
        "password VARCHAR"
        ")");
  }

  //cek apakah versi database diupgrade
  void _onUpgrade(Database database, int oldVersion, int newVersion) {
    if (newVersion > oldVersion) {}
  }
}
