import 'package:majootestcase/models/user.dart';

abstract class UserDataSource {
  Future<int> register(String username, String email, String password);
  Future<User> login(String email, String password);
}
