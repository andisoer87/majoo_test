import 'package:intl/intl.dart';

String formatDate(String format, String input) {
  var inputFormat = DateFormat("yyyy-MM-dd");
  var inputDate = inputFormat.parse(input);

  var outputFormat = DateFormat(format);
  var outputDate = outputFormat.format(inputDate);

  return outputDate;
}