class Api {
  static const BASE_URL = "https://api.themoviedb.org/3/movie/";
  static const BASE_URL_IMAGE = "https://image.tmdb.org/t/p/";
  static const URL_POSTER_W500 = BASE_URL_IMAGE + "w500";
  static const URL_POSTER_W154 = BASE_URL_IMAGE + "w154";
  static const URL_BACKDROP_W780 = BASE_URL_IMAGE + "w780";
  static const API_KEY = "cd4201de3a48a9eadba7e82bd579036c";
}
