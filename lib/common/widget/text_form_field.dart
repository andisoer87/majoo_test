import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final TextInputType textInputType;
  final String hintText;
  final bool isObscurePassword;
  final void Function(String) onFieldSubmitted;
  final Color fillColor;
  final TextInputAction textInputAction;

  const CustomTextFormField({
    Key key,
    this.controller,
    this.validator,
    this.suffixIcon,
    this.textInputType,
    this.hintText,
    this.isObscurePassword = false,
    this.onFieldSubmitted,
    this.fillColor,
    this.textInputAction,
    this.prefixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: textInputType,
      validator: validator,
      obscureText: isObscurePassword,
      onFieldSubmitted: onFieldSubmitted,
      textInputAction: textInputAction,
      decoration: InputDecoration(
        hintText: hintText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
          borderSide: BorderSide.none,
        ),
        fillColor: fillColor != null ? fillColor : Colors.grey.withOpacity(0.5),
        filled: true,
        suffixIcon: suffixIcon,
        prefixIcon: prefixIcon,
      ),
    );
  }
}
