import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/data/local/user_repository.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

/// Class berfungsi untuk operasi login register user ke database
/// Author @ Andi Surya
/// Hasil berupa state dari manupulasi data user
class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  //ambil login session
  void fetchLoginSession() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  //login user dari database
  void loginUser(String email, String password) async {
    emit(AuthBlocLoadingState());

    var userRepository = UserRepository();
    var user = await userRepository.login(email, password);

    if (user != null) {
      _saveUserSession(user);
    } else {
      emit(AuthBlocErrorState('Login gagal , periksa kembali inputan anda'));
    }
  }

  //register user ke database
  void registerUser(String username, String email, String password) async {
    emit(AuthBlocLoadingState());

    var userRepository = UserRepository();
    var insertedId = await userRepository.register(username, email, password);

    var user = User(
      id: insertedId,
      username: username,
      email: email,
      password: password,
    );

    if (insertedId != null) {
      _saveUserSession(user);
    } else {
      emit(AuthBlocLoginState());
    }
  }

  //simpan user session setelah login
  void _saveUserSession(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", true);
    String data = user.toJson().toString();
    sharedPreferences.setString("user_value", data);
    emit(AuthBlocLoggedInState());
  }

  //clear user session (logout)
  void clearUserSession() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", false);
    sharedPreferences.setString("user_value", null);
    emit(AuthBlocLoginState());
  }
}
