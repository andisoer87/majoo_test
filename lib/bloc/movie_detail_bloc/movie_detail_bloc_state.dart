part of 'movie_detail_bloc_cubit.dart';

abstract class MovieDetailBlocState extends Equatable {
  const MovieDetailBlocState();

  @override
  List<Object> get props => [];
}

class MovieDetailBlocInitialState extends MovieDetailBlocState {}

class MovieDetailBlocLoadingState extends MovieDetailBlocState {}

class MovieDetailBlocLoadedState extends MovieDetailBlocState {
  final MovieDetailResponse movieDetail;
  MovieDetailBlocLoadedState(this.movieDetail);
  @override
  List<Object> get props => [movieDetail];
}

class MovieDetailBlocErrorState extends MovieDetailBlocState {
  final error;

  MovieDetailBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
