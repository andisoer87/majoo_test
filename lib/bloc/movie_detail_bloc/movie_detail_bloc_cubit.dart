import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_detail_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'movie_detail_bloc_state.dart';

/// Class berfungsi untuk ambil data untuk movie_detail_bloc_screen
/// Author @ Andi Surya
/// Hasil berupa state dari ambil data get movie list
class MovieDetailBlocCubit extends Cubit<MovieDetailBlocState> {
  MovieDetailBlocCubit() : super(MovieDetailBlocInitialState());

  //ambil movie detail
  void fetchMovieDetail(int movieId) async {

    //check koneksi internet
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      emit(MovieDetailBlocErrorState("Periksa koneksi anda"));
      return;
    }

    emit(MovieDetailBlocLoadingState());
    ApiServices apiServices = ApiServices();
    MovieDetailResponse movieResponse =
        await apiServices.getMovieDetail(movieId); //ambil movie detail
    if (movieResponse == null) {
      emit(MovieDetailBlocErrorState("Error Unknown"));
    } else {
      emit(MovieDetailBlocLoadedState(movieResponse));
    }
  }
}
