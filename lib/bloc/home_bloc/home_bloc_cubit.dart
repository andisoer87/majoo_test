import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

/// Class berfungsi untuk ambil data untuk home_bloc_screen
/// Author @ Andi Surya
/// Hasil berupa state dari ambil data get movie list
class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  List<Movie> movieList = [];
  List<Movie> searchedMovies = [];

  //ambil movie list
  void fetchMovieData() async {
    //check koneksi internet
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      emit(HomeBlocErrorState("Periksa koneksi anda"));
      return;
    }

    emit(HomeBlocLoadingState());
    ApiServices apiServices = ApiServices();
    MovieResponse movieResponse =
        await apiServices.getMovieList(); //ambil dari api getMovieList()
    if (movieResponse == null) {
      emit(HomeBlocErrorState("Error Unknown"));
    } else {
      movieList = movieResponse.results;
      searchedMovies.addAll(movieList);
      emit(HomeBlocLoadedState(searchedMovies));
    }
  }

  //search movie list berdasarkan nama
  void searchMovie(String query) async {
    emit(HomeBlocLoadingState());
    searchedMovies.clear();
    movieList.forEach((movie) {
      if (movie.title.toLowerCase().contains(query)) {
        searchedMovies.add(movie);
      }
    });

    if (searchedMovies.isEmpty) {
      emit(HomeBlocLoadedState(movieList));
    } else {
      emit(HomeBlocLoadedState(searchedMovies));
    }
  }

  void sortMovieList(String order) async {
    emit(HomeBlocLoadingState());
    if (order == 'asc') {
      searchedMovies.sort((firstItem, secondItem) =>
          firstItem.title.compareTo(secondItem.title));
    } else {
      searchedMovies.sort((firstItem, secondItem) =>
          secondItem.title.compareTo(firstItem.title));
    }

    emit(HomeBlocLoadedState(searchedMovies));
  }
}
